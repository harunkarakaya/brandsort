﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSort
{
    class Program
    {
        static void Main(string[] args)
        {
            string AnswerMain = "Evet";

            while (AnswerMain == "Evet" || AnswerMain == "evet")
            {
                Console.WriteLine("Kaç karakterden büyük araba markalarını görmek istiyorsunuz?");
                int NumofCharLarge = Int32.Parse(Console.ReadLine());

                IList<string> brands = new List<string>() { "BMW", "Audi", "Porsche", "Mercedes", "Bentley", "Opel", "Ford", "Fiat", "Mazda", "Volkswagen", "Volvo", "Ferrari" };

                brands = GetBrands(brands, NumofCharLarge);

                WriteBrands(brands);

                AnswerMain = Question();
            }


        }

        public static IList<string> GetBrands(IList<string> brands, int NoCL)
        {
            IList<string> sequentialCars = new List<string>();
            brands = brands.OrderBy(x => x.ToString()).ToList();

            for (int i = 0; i < brands.Count; i++)
            {
                if (brands[i].Length > NoCL)
                {
                    if (i + 1 == brands.Count)
                    {
                        sequentialCars.Add(brands[i]);
                    }
                    else
                    {
                        sequentialCars.Add(brands[i] + ",");
                    }
                }
            }

            return sequentialCars;
        }

        public static void WriteBrands(IList<string> brands)
        {
            foreach (string car in brands)
            {
                Console.Write(car);
            }
            Console.Write("\n");
        }

        public static string Question()
        {
            string Answer = "";
            Console.WriteLine("\nİşlemi tekrarlamak istiyor musunuz?");
            Console.WriteLine("Evet/Hayır");
            Answer = Console.ReadLine();

            return Answer;
        }
    }
}
